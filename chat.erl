-module(chat).
%-export([start/0, loop/1]).
-compile(export_all).

start() ->
	register(pUsers, spawn(?MODULE,userdb,[ #{} ] )),
	add_user("user1", "sifre", normal),
	add_user("user2", "sifre", normal),
	add_user("user3", "sifre", normal),
%	add_user("bot1", "", bot),
	socket_server:start(?MODULE, 7000, {?MODULE, loop}).

loop(Socket) ->
    case gen_tcp:recv(Socket, 0) of
        {ok, Data} ->
						[H|T] = binary:bin_to_list(Data),
						if
							H==1 ->
								[IUser|IPass] = splitList(29, T),
								io:fwrite("Username:~p \n", [IUser]),
								io:fwrite("Password:~p \n", [IPass]),
								case check_password(IUser, lists:flatten(IPass)) of
									true->
										set_user_status(IUser, Socket, online),
										gen_tcp:send(Socket, [10]),
										io:fwrite("User ~s Logged in\n", [IUser]),
										loopLoggedIn(Socket, IUser);
									false->
										io:fwrite("No match for ~s : ~s\n", [IUser, IPass]),
										gen_tcp:send(Socket, [11])
								end;
							true ->
								loop(Socket)
						end;
        {error, closed} ->
            ok
    end.

loopLoggedIn(Socket, Username)->
	case gen_tcp:recv(Socket, 0) of
			{ok, Data} ->
					[H|T] = binary:bin_to_list(Data),
					if
						H==2 -> % private message
							[MTo, MContent] = splitList(29, T),
							send_message(Username, MTo, MContent),
							ok;
						H==3 -> % get users
							gen_tcp:send(Socket, generatePayload(3, get_all_users())),
							ok;
						H==5 -> % broadcast message
							[MFrom, MContent] = splitList(29, T),
							send_room_message(MFrom, MContent),
							ok;
						true ->
							ok
					end,
					loopLoggedIn(Socket, Username);
			{error, closed} ->
					set_user_status(Username, undefined, offline),
					ok
	end.
% Utils
splitList(_, []) -> [];
splitList(Delimiter, List)->
  [H|T] = List,
  case H==Delimiter of
    true->
      {NH, NT} = lists:splitwith(fun(A) -> A/=Delimiter end , T);
    false->
      {NH, NT} = lists:splitwith(fun(A) -> A/=Delimiter end , List)
  end,
  NewList = [NH] ++ splitList(Delimiter, NT),
  NewList.

generatePayload(Identifier, List)->
  [Identifier] ++ generatePayload2(List).

generatePayload2([]) -> [];
generatePayload2(List)->
  [H|T] = List,
  case T of
    []-> H;
    _ -> H ++ [29] ++ generatePayload2(T)
  end.

prettyPrintUser(K, V)->
	{Password, Role, _ , IsOnline, _ } = V,
	io:format("~s : password(~s), role(~s), status(~s) \n", [K, Password, atom_to_list(Role), atom_to_list(IsOnline)]).
% Users Database.
% UserName, Password, Role, UserPid, IsOnline, SocketR
userdb(Users) ->
  receive
		{addUser, {Username, Password, Role}} ->
			PIDUser = spawn(?MODULE, user_process, [Username, offline]),
			NewUsers = Users#{Username=>{Password, Role, PIDUser, offline, undefined}},
			userdb(NewUsers),
			ok;

		{removeUser, {Username}} ->
			NewUsers = maps:remove(Username, Users),
			userdb(NewUsers),
			ok;

		{userExist, {Requester, Username}} ->
			Requester ! maps:is_key(Username, Users),
			userdb(Users),
			ok;

		{checkUserCred, {Requester, Username, Password}} ->
			case maps:is_key(Username, Users) of
				true ->
					{RealPassword, _, _, _, _} = maps:get(Username, Users),
					Requester ! RealPassword==Password;
				false -> Requester ! false
			end,
			userdb(Users),
			ok;

		{setUserStatus, {Requester, Username, NewSocketR, NewStatus}} ->
			case maps:is_key(Username, Users) of
				true ->
					{Password, Role, UserPid , _, _ } = maps:get(Username, Users),
					NewUsers = Users#{Username=>{Password, Role, UserPid, NewStatus, NewSocketR}},
					UserPid ! {statusChange, {NewStatus}},
					Requester ! true,
					userdb(NewUsers);
				false ->
					Requester ! false,
					userdb(Users)
			end,
			ok;

		{printUsers} ->
			maps:map(fun(K, V) -> prettyPrintUser(K, V) end, Users),
			userdb(Users),
			ok;

		{getUsers, {Requester}} ->
			Requester ! maps:keys(Users),
			userdb(Users),
			ok;

		{getPids, {Requester, Username}} ->
			case maps:is_key(Username, Users) of
				true ->
					{_, _, UserPid , _, SocketR } = maps:get(Username, Users),
					Requester ! {UserPid, SocketR},
					userdb(Users);
				false ->
					Requester ! {undefined, undefined},
					userdb(Users)
			end,
			ok;

		{getRole, {Requester, Username}} ->
			case maps:is_key(Username, Users) of
				true ->
					{_, UserRole, _ , _, _ } = maps:get(Username, Users),
					Requester ! {UserRole},
					userdb(Users);
				false ->
					Requester ! {undefined, undefined},
					userdb(Users)
			end,
			ok;
		{close} ->
			ok
  end.

add_user(Username, Password, Role)->
	pUsers ! {addUser, {Username, Password, Role}}.

remove_user(Username)->
	pUsers ! {removeUser, {Username}}.

print_users()->
	pUsers ! {printUsers}.

is_user_exist(Username)->
	pUsers ! {userExist, {self(), Username}},
	receive
		Response->
			Response
	end.

check_password(Username, Password)->
	pUsers ! {checkUserCred, {self(), Username, Password}},
	receive
		Response->
			Response
	end.

set_user_status(Username, SocketR, Status) ->
	pUsers ! {setUserStatus, {self(), Username, SocketR, Status}},
	receive
		Response->
			Response
	end.

get_all_users()->
	pUsers ! {getUsers, {self()}},
	receive
		Response->
			Response
	end.

get_pids(Username) ->
	pUsers ! {getPids, {self(), Username}},
	receive
		Response->
			Response
	end.

get_user_role(Username) ->
	pUsers ! {getRole, {self(), Username}},
	receive
		Response->
			Response
	end.

% User Process
user_process(Username, UserStatus)->
	receive
		{newMessage, {From, MessageContent}} ->
			case UserStatus of
				offline->
					io:format("New Offline Message(~s): ~s\n", [From, MessageContent]),
					user_process(Username, UserStatus);
				online->
					io:format("New Online Message(~s): ~s\n", [From, MessageContent]),
					{_, SocketR} = get_pids(Username),
					gen_tcp:send(SocketR, generatePayload(2, [From]++ [MessageContent])),
					user_process(Username, UserStatus)
			end;

		{newRoomMessage, {From, MessageContent}} ->
			{_, SocketR} = get_pids(Username),
			% {GRole} = get_user_role(Username),
			% case GRole of
			% 	bot->
			% 		case MessageContent=="hello everyone" of
			% 			true->
			% 				send_room_message(Username, ["Hello "] ++ From);
			% 			false-> ok
			% 		end;
			% 	normal-> ok
			% end,
			case SocketR of
				undefined-> ok;
				_ -> gen_tcp:send(SocketR, generatePayload(5, [From]++ [MessageContent]))
		  end,

			user_process(Username, UserStatus);

		{statusChange, {NewStatus}} ->
			io:format("~s status changed to ~s\n", [Username, atom_to_list(NewStatus)]),
			user_process(Username, NewStatus)
	end.

send_message(UserFrom, UserTo, Message)->
	{UserPid, SocketR} = get_pids(UserTo),
	UserPid ! {newMessage, {UserFrom, Message}}.

send_room_message(UserFrom, Message)->
	Users = get_all_users(),
	lists:map(fun(X) ->
										{UserPid, _} = get_pids(X),
										UserPid ! {newRoomMessage, {UserFrom, Message}}
	  							end, Users).
