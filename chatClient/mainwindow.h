#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QtNetwork>
#include <QListWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  QTcpSocket socket;
  bool isLoggedin;
  QMap<QString, QStringList> messages;

private slots:
  void on_lineEditInput_returnPressed();

  void on_pushButton_clicked();

  void on_listWidgetContacts_itemSelectionChanged();

  void on_listWidgetContacts_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

public slots:
  void onNewPacket();

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
