#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  QObject::connect(&socket, SIGNAL(readyRead()), this, SLOT(onNewPacket()));

  ui->lineEditPassword->setEchoMode(QLineEdit::Password);

  ui->stackedWidget->setCurrentIndex(0);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::on_lineEditInput_returnPressed()
{
    QString fromUser = ui->lineEditUsername->text();
    QString message = ui->lineEditInput->text();
    QString toUser = ui->listWidgetContacts->currentItem()->text();

    messages[toUser].push_back(QString("<b>%1</b> : %2<br>").arg(fromUser).arg(message));
    ui->lineEditInput->clear();
    ui->textEditChatWindow->insertHtml(messages[toUser].last());

    if (ui->listWidgetContacts->currentItem()->text().compare("Room")==0){
      QByteArray arr;
      arr.append(5);
      arr.append(fromUser);
      arr.append(29);
      arr.append(message);
      socket.write(arr);
    } else {
      QByteArray arr;
      arr.append(2);
      arr.append(toUser);
      arr.append(29);
      arr.append(message);
      socket.write(arr);
    }


}

void MainWindow::on_pushButton_clicked()
{
  QByteArray packet;

  packet.append(1);
  packet.append(ui->lineEditUsername->text().toLocal8Bit());
  packet.append(29);
  packet.append(ui->lineEditPassword->text().toLocal8Bit());

  socket.connectToHost(ui->lineEditHost->text(), ui->spinBoxPort->value());
  socket.waitForConnected();
  socket.write(packet);
}

void MainWindow::onNewPacket()
{
  QByteArray arr = socket.readAll();

  if (arr.at(0)==10){
      qDebug() << "Login Success";
      ui->labelStatus->setText("");
      isLoggedin=true;
      QByteArray getusersarr;
      getusersarr.append(3);
      socket.write(getusersarr);
      ui->stackedWidget->setCurrentIndex(1);
    }

    if (arr.at(0)==11){
      ui->labelStatus->setText("Wrong Username/Password");
      socket.disconnectFromHost();
    }

    if (arr.at(0)==3){
      arr.remove(0,1);
      QList<QByteArray> splitted = arr.split(29);
      splitted.removeAll(ui->lineEditUsername->text().toLocal8Bit());
      ui->listWidgetContacts->clear();
      ui->listWidgetContacts->addItem("Room");
      for (int i=0; i < splitted.count(); i++){
        ui->listWidgetContacts->addItem(splitted.at(i));
      }
      ui->listWidgetContacts->setCurrentRow(0);
    }

    if (arr.at(0)==2){
      arr.remove(0,1);
      QList<QByteArray> splitted = arr.split(29);
      if (splitted.count()==2) {
        QString fromUser = splitted.at(0);
        QString message = splitted.at(1);
        messages[fromUser].push_back(QString("<b>%1</b>: %2<br>").arg(fromUser).arg(message));
        if (fromUser.compare(ui->listWidgetContacts->currentItem()->text())==0){
          ui->textEditChatWindow->insertHtml(messages[fromUser].last());
        } else {
          QList<QListWidgetItem *> items = ui->listWidgetContacts->findItems(fromUser, Qt::MatchExactly);
          if (items.size() > 0) {
              items.at(0)->setForeground(Qt::green);
          }
        }
      } else {
        qDebug() << "invalid data(incoming message)";
      }
    }

    if (arr.at(0)==5){
      arr.remove(0,1);
      QList<QByteArray> splitted = arr.split(29);
      if (splitted.count()==2) {
        QString fromUser = splitted.at(0);
        QString message = splitted.at(1);
        if (fromUser.compare(ui->lineEditUsername->text())==0 ) return;
        messages["Room"].push_back(QString("<b>%1</b>: %2<br>").arg(fromUser).arg(message));
        if (ui->listWidgetContacts->currentItem()->text().compare("Room")==0){
          ui->textEditChatWindow->insertHtml(messages["Room"].last());
        } else {
          QList<QListWidgetItem *> items = ui->listWidgetContacts->findItems("Room", Qt::MatchExactly);
          if (items.size() > 0) {
              items.at(0)->setForeground(Qt::green);
          }
        }
      } else {
        qDebug() << "invalid data(incoming message)";
      }
    }

}

void MainWindow::on_listWidgetContacts_itemSelectionChanged()
{
  //qDebug() << "Item Changed";
  ui->textEditChatWindow->clear();
  QStringList& tmpList = messages[ui->listWidgetContacts->currentItem()->text()];
  for (int i=0; i < tmpList.count(); i++){
    ui->textEditChatWindow->insertHtml(tmpList.at(i));
  }

  ui->listWidgetContacts->currentItem()->setForeground(Qt::black);
}

void MainWindow::on_listWidgetContacts_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
}
